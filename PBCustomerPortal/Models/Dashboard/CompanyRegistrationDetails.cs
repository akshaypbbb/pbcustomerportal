﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PBCustomerPortal.Models.Dashboard
{
    public class CompanyRegistrationDetails
    {
            public int CompanyId { get; set; }
            [Display(Name = "Company Name")]
            [Required(ErrorMessage = "Company Name is required.")]
            public string CompanyName { get; set; }
            public int UserId { get; set; }
            [Display(Name = "Redirect URL")]
            [Required(ErrorMessage = "Redirect URL is required.")]
            public string RedirectURL { get; set; }
            public int ImageId { get; set; }
            public string Title { get; set; }
            [Display(Name = "Image Path")]
            [Required(ErrorMessage = "Image Path is required.")]
            public string ImagePath { get; set; }

    }
}
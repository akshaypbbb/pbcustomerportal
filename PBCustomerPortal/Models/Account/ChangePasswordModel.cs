﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PBCustomerPortal.Models.Account
{
    public class ChangePasswordModel
    {
        [Display(Name = " Old Password")]
        [Required(ErrorMessage ="Old Pasword is required.")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }
        [Display(Name = " New Password")]
        [Required(ErrorMessage = "New Pasword is required.")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = " Confirm Password")]
        [Required(ErrorMessage = "Confirm New Pasword is required.")]
        [DataType(DataType.Password)]
        [Compare(otherProperty:"NewPassword",ErrorMessage ="New Password doesnt match.")]
        public string ConfirmNewPassword { get; set; }
    }
}
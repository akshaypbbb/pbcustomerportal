﻿using PBCustomerPortal.Models.General;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PBCustomerPortal.Models.Account;
using WebMatrix.WebData;

namespace PBCustomerPortal.ViewModels.Account
{
    public class AccountViewModels
    {
        public static List<SelectListItem> GetAllRoles(int roleId)
        {
            List<SelectListItem> roles = new List<SelectListItem>();
            using (SqlConnection con = new SqlConnection(AppSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("RolesGetRolesByRoleId", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();
                    cmd.Parameters.AddWithValue("@RoleId", roleId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        SelectListItem item = new SelectListItem();
                        item.Value = reader["RoleName"].ToString();
                        item.Text = reader["RoleName"].ToString();
                        roles.Add(item);
                    }
                    con.Close();
                }
            }
                return roles;
        }

        public static UserProfileModel GetUserProfileData(int currentUserId)
        {
            UserProfileModel userProfileModel = new UserProfileModel();
            using (SqlConnection con = new SqlConnection(AppSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("UsersGetUserProfileData", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();
                    cmd.Parameters.AddWithValue("@UserId", currentUserId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    reader.Read();
                    userProfileModel.FullName = reader["FullName"].ToString();
                    userProfileModel.Email = reader["Email"].ToString();
                    con.Close();
                }

            }
            return userProfileModel;
        }

        public static void UpdateUserProfile(UserProfileModel userProfile)
        {
            using (SqlConnection con = new SqlConnection(AppSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("UsersUpdateUserProfileData", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();
                    cmd.Parameters.AddWithValue("@UserId", WebSecurity.CurrentUserId);
                    cmd.Parameters.AddWithValue("@FullName", userProfile.FullName);
                    cmd.Parameters.AddWithValue("@Email", userProfile.Email);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        public static List<UserModel> GetAllUsers()
        {
            List<UserModel> users = new List<UserModel>();
            using (SqlConnection con = new SqlConnection(AppSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("UsersGetAllUsers", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        UserModel user = new UserModel();
                        user.UserId = Convert.ToInt32(reader["UserId"]);
                        user.UserName = reader["UserName"].ToString();
                        user.FullName = reader["FullName"].ToString();
                        user.Email = reader["Email"].ToString();
                        users.Add(user);
                    }
                    con.Close();
                }
            }
            return users;
        }
    }
}
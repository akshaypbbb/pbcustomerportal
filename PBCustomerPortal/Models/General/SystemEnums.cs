﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PBCustomerPortal.Models.General
{
   public enum Role
    {
        NoRole = 0,
        Administrator =1,
        SuperUser =2,
        Normal =3
    }
}
﻿using PBCustomerPortal.Models.Account;
using PBCustomerPortal.Models.General;
using PBCustomerPortal.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace PBCustomerPortal.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel loginModel)
        {
            if(ModelState.IsValid)
            {
                bool isAuthenticated = WebSecurity.Login(loginModel.UserName,loginModel.Password,loginModel.RememberMe);
                if (isAuthenticated)
                {
                    return RedirectToAction("Index", "Dashboard");
                }
            }else
            {
                ModelState.AddModelError("","User name or password is invalid.");
            }
           
            return View();
        }

        public ActionResult SignOut()
        {
            WebSecurity.Logout();
            return RedirectToAction("Login", "Account");
        }
        [HttpGet]
        [Authorize]
        public ActionResult Register()
        {
            GetRolesForCurrentUser();
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult Cancel()
        {
            GetRolesForCurrentUser();
            return View();
        }

        private void GetRolesForCurrentUser()
        {
            if (Roles.IsUserInRole(WebSecurity.CurrentUserName, "Administrator"))
            {
                ViewBag.RoleId = (int)Role.Administrator;
            }
            else
            {
                ViewBag.RoleId = (int)Role.NoRole;
            }
        }

        [HttpPost,ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Register(RegisterModel registerModel)
        {
            GetRolesForCurrentUser();
            if(ModelState.IsValid)
            {
                bool isUserExist = WebSecurity.UserExists(registerModel.UserName);
                if(isUserExist)
                {
                    ModelState.AddModelError("UserName","User Name already exist.");
                }
                else
                {
                    WebSecurity.CreateUserAndAccount(registerModel.UserName,registerModel.Password,new { FullName = registerModel.FullName , Email=registerModel.Email});
                    Roles.AddUserToRole(registerModel.UserName, registerModel.Role);
                    return RedirectToAction("Index", "Dashboard");
                }
            }
            return View();
        }

        [HttpGet,Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost, Authorize,ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel changePassModel)
        {
            if(ModelState.IsValid)
            {
                bool isPasswordChanged = WebSecurity.ChangePassword(WebSecurity.CurrentUserName, changePassModel.OldPassword, changePassModel.NewPassword);

                if(isPasswordChanged)
                {
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {
                    ModelState.AddModelError("OldPassword","Old Password is not correct.");
                }
            }
            return View();
        }

        [HttpGet,Authorize]
        public ActionResult UserProfile()
        {
            UserProfileModel userProfileModel = AccountViewModels.GetUserProfileData(WebSecurity.CurrentUserId);
            return View(userProfileModel);
        }

        [HttpPost,Authorize,ValidateAntiForgeryToken]
        public ActionResult UserProfile(UserProfileModel userProfileModel)
        {
            if(ModelState.IsValid)
            {
                AccountViewModels.UpdateUserProfile(userProfileModel);
                ViewBag.Message = "User Profile data updated successfully.";
                //ModelState.AddModelError("OldPassword", "Old Password is not correct.");
            }
            else
            {
              //  ModelState.AddModelError("OldPassword", "Old Password is not correct.");
            }
            return View();
        }
        [HttpGet,Authorize(Roles ="Administrator,SuperUser")]
        public ActionResult UserList()
        {
            List<UserModel> users = AccountViewModels.GetAllUsers();
            return View(users);
        }


    }
}